{-# options_ghc
  -Wno-missing-export-lists
  -Wno-missing-import-lists
  -Wno-orphans
  #-}

{-# language UndecidableInstances #-}

module Generics.Simple.Examples where

import Generics.Simple
import GHC.Generics (Generic)

-------------------------------------------------------------------------------
type List a = Data (I + T a * F)

deriving via Blueprint [a] (I + T a * F) instance Shaped [a]

--------------------------------------------------------------------------------
newtype Fix f = Fix (f (Fix f)) deriving Generic

deriving instance (forall a. Show (f a)) => Show (Fix f)

type Fix' f = Data (F1 f)

deriving via Blueprint (Fix f) (F1 f) instance Functor f => Shaped (Fix f)

-------------------------------------------------------------------------------
type Maybe' a = Data (I + T a)

deriving anyclass instance Shaped (Maybe a)

-------------------------------------------------------------------------------
infixr 4 :?
data Whatever a = WNil | (Int, Maybe a) :? Whatever a deriving Generic

deriving via Blueprint (Whatever a) (I + T Int * (I + T a) * F)
  instance Shaped (Whatever a)