{-# language
    UndecidableInstances
  #-}

module Generics.Simple.Dynamic where

import Control.Monad  (join)
import Data.Bifunctor (Bifunctor (bimap))
import Data.Kind      (Type, Constraint)
import Generics.Simple

-------------------------------------------------------------------------------
type DData s = DDataOf s s

data DDataOf :: Shape -> Shape -> Type where
  DT    :: a                          -> DDataOf f (T a)
  DF    :: DData f                    -> DDataOf f  F
  DF1   :: t (DData f)                -> DDataOf f (F1 t)
  DF2   :: t (DData f) (DData f)      -> DDataOf f (F2 t)
  DI    ::                               DDataOf f  I
  (:%:) :: DDataOf f s -> DDataOf f t -> DDataOf f (s * t)
  DL    :: DDataOf f s                -> DDataOf f (s + t)
  DR    ::                DDataOf f t -> DDataOf f (s + t)
        --                               DDataOf f  O

deriving instance ConstrainDData Show f s => Show (DDataOf f s)

type family ConstrainDData (c :: Type -> Constraint)
                           (f :: Shape)
                           (s :: Shape)
                        :: Constraint where
  ConstrainDData c _ (T  a)  = c a
  ConstrainDData c f  F      = c (DData f)
  ConstrainDData c f (F1 t)  = c (t (DData f))
  ConstrainDData c f (F2 t)  = c (t (DData f) (DData f))
  ConstrainDData c f (s * t) = (ConstrainDData c f s, ConstrainDData c f t)
  ConstrainDData c f (s + t) = (ConstrainDData c f s, ConstrainDData c f t)
  ConstrainDData _ _  _      = ()

-------------------------------------------------------------------------------
class ToDData f s where
  toDData   :: DataOf f s -> DDataOf f s
  fromDData :: DDataOf f s -> DataOf f s

instance ToDData f (T a) where
  toDData   (T  a) = DT a
  fromDData (DT a) = T  a

instance ToDData f f => ToDData f F where
  toDData   (F  d) = DF $ toDData   d
  fromDData (DF d) = F  $ fromDData d

instance (ToDData f f, Functor t) => ToDData f (F1 t) where
  toDData   (F1  t) = DF1 $ toDData   <$> t
  fromDData (DF1 t) = F1  $ fromDData <$> t

instance (ToDData f f, Bifunctor t) => ToDData f (F2 t) where
  toDData   (F2  t) = DF2 $ join bimap toDData   t
  fromDData (DF2 t) = F2  $ join bimap fromDData t

instance ToDData f I where
  toDData   I  = DI
  fromDData DI = I

instance (ToDData f s, ToDData f t) => ToDData f (s * t) where
  toDData   (l :*: r) = toDData   l :%: toDData   r
  fromDData (l :%: r) = fromDData l :*: fromDData r

instance (ToDData f s, ToDData f t) => ToDData f (s + t) where
  toDData   (L  l) = DL $ toDData   l
  toDData   (R  r) = DR $ toDData   r
  fromDData (DL l) = L  $ fromDData l
  fromDData (DR r) = R  $ fromDData r

instance ToDData f O where
  toDData   = \case {}
  fromDData = \case {}