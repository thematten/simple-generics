module Generics.Simple.Properties (
    leftPlusId, rightPlusId, plusComm
  ) where

import Generics.Simple

-------------------------------------------------------------------------------
leftPlusId :: NonRec b => Data (O + b) -> Data b
leftPlusId (R a) = refix a
leftPlusId (L o) = case o of {}

-------------------------------------------------------------------------------
rightPlusId :: NonRec a => Data (a + O) -> Data a
rightPlusId (R o) = case o of {}
rightPlusId (L b) = refix b

-------------------------------------------------------------------------------
plusComm :: (NonRec a, NonRec b) => Data (a + b) -> Data (b + a)
plusComm (L a) = R $ refix a
plusComm (R b) = L $ refix b

-------------------------------------------------------------------------------
-- plusAssoc :: (NonRec a, Non)