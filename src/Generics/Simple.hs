{-# language
    AllowAmbiguousTypes
  , UndecidableInstances
  #-}

module Generics.Simple (
    Shape (..)
  , type T, type F, type F1, type F2, type I, type (*), type (+), type O
  , Data, DataOf (..)
  , Shaped (..)
  , Blueprint (..)
  , NonRec
  , refix
  ) where

import           Control.Monad  (join)
import           Data.Bifunctor (Bifunctor (bimap))
import           Data.Kind      (Type)
import qualified GHC.Generics as G
import           GHC.TypeLits   (TypeError, ErrorMessage (Text, ShowType, (:<>:), (:$$:)))

-------------------------------------------------------------------------------
infixl 6 +
infixl 7 *

-------------------------------------------------------------------------------
data Shape = T'   Type
           | F'
           | F1' (Type -> Type)
           | F2' (Type -> Type -> Type)
           | I'
           | Shape :* Shape
           | Shape :+ Shape
           | O'

type T   = ' T'
type F   = ' F'
type F1  = ' F1'
type F2  = ' F2'
type I   = ' I'
type (*) = '(:*)
type (+) = '(:+)
type O   = ' O'

-------------------------------------------------------------------------------
type Data s = DataOf s s

data    family   DataOf :: Shape -> Shape -> Type
newtype instance DataOf f (T a)   = T a
newtype instance DataOf f  F      = F  (Data f)
newtype instance DataOf f (F1 t)  = F1 (t (Data f))
newtype instance DataOf f (F2 t)  = F2 (t (Data f) (Data f))
data    instance DataOf f  I      = I
data    instance DataOf f (a * b) = DataOf f a :*: DataOf f b
data    instance DataOf f (a + b) = L (DataOf f a) | R (DataOf f b)
data    instance DataOf f O

unT  :: DataOf f (T a) -> a
unT  (T  a) = a
unF  :: DataOf f F -> Data f
unF  (F  d) = d
unF1 :: DataOf f (F1 t) -> t (Data f)
unF1 (F1 t) = t
unF2 :: DataOf f (F2 t) -> t (Data f) (Data f)
unF2 (F2 t) = t

deriving instance (Show (DataOf f s), Show (DataOf f t))
               => Show (DataOf f (s + t))
deriving instance (Show (DataOf f s), Show (DataOf f t))
               => Show (DataOf f (s * t))
deriving instance Show a
               => Show (DataOf f (T a))
deriving instance Show (DataOf f I)
deriving instance Show (DataOf f O)
deriving instance Show (Data f)
               => Show (DataOf f F)
deriving instance Show (t (Data f))
               => Show (DataOf f (F1 t))
deriving instance Show (t (Data f) (Data f))
               => Show (DataOf f (F2 t))

-------------------------------------------------------------------------------
class Shaped a where
  type ShapeOf a :: Shape
  to   :: a -> Data (ShapeOf a)
  from :: Data (ShapeOf a) -> a

  type ShapeOf a = GShapeOf (G.Rep a)

  default to ::
              ( G.Generic a
              , GShaped (G.Rep a)
              , ShapeOf a ~ GShapeOf (G.Rep a)
              )
             => a -> Data (ShapeOf a)
  to = gTo . G.from

  default from ::
                ( G.Generic a
                , GShaped (G.Rep a)
                , ShapeOf a ~ GShapeOf (G.Rep a)
                )
               => Data (ShapeOf a) -> a
  from = G.to . gFrom

-------------------------------------------------------------------------------
class GShaped g where
  type GShapeOf g :: Shape
  gTo   :: g p -> DataOf f (GShapeOf g)
  gFrom :: DataOf f (GShapeOf g) -> g p

instance GShaped g => GShaped (G.M1 t i g) where
  type GShapeOf (G.M1 t i g) = GShapeOf g
  gTo   = gTo . G.unM1
  gFrom = G.M1 . gFrom

instance GShaped G.V1 where
  type GShapeOf G.V1 = O
  gTo   = \case {}
  gFrom = \case {}

instance (GShaped g, GShaped h) => GShaped (g G.:+: h) where
  type GShapeOf (g G.:+: h) = GShapeOf g + GShapeOf h
  gTo   (G.L1 g) = L    $ gTo   g
  gTo   (G.R1 h) = R    $ gTo   h
  gFrom (L    l) = G.L1 $ gFrom l
  gFrom (R    r) = G.R1 $ gFrom r

instance (GShaped g, GShaped h) => GShaped (g G.:*: h) where
  type GShapeOf (g G.:*: h) = GShapeOf g * GShapeOf h
  gTo   (g G.:*: h) = gTo   g  :*:  gTo   h
  gFrom (l  :*:  r) = gFrom l G.:*: gFrom r

instance GShaped G.U1 where
  type GShapeOf G.U1 = I
  gTo   _ = I
  gFrom _ = G.U1

instance GShaped (G.Rec0 t) where
  type GShapeOf (G.Rec0 t) = T t
  gTo   = T . G.unK1
  gFrom = G.K1 . unT

-------------------------------------------------------------------------------
newtype Blueprint :: Type -> Shape -> Type where
  B :: { unB :: a } -> Blueprint a s

deriving via (a :: Type) instance G.Generic a => G.Generic (Blueprint a s)

instance (G.Generic a, BShaped a s (G.Rep a) s) => Shaped (Blueprint a s) where
  type ShapeOf (Blueprint a s) = s
  to   = bTo @a . G.from
  from = G.to . bFrom @a

-------------------------------------------------------------------------------
class BShaped rT rS g s where
  bTo   :: g p -> DataOf rS s
  bFrom :: DataOf rS s -> g p

instance BShaped rT rS g s => BShaped rT rS (G.M1 t i g) s where
  bTo   = bTo @rT . G.unM1
  bFrom = G.M1 . bFrom @rT

instance BShaped rT rS G.V1 O where
  bTo   = \case {}
  bFrom = \case {}

instance (BShaped rT rS g s, BShaped rT rS h t)
      => BShaped rT rS (g G.:+: h) (s + t) where
  bTo   (G.L1 g) = L    $ bTo   @rT g
  bTo   (G.R1 h) = R    $ bTo   @rT h
  bFrom (L    l) = G.L1 $ bFrom @rT l
  bFrom (R    r) = G.R1 $ bFrom @rT r

instance (BShaped rT rS g s, BShaped rT rS h t)
      => BShaped rT rS (g G.:*: h) (s * t) where
  bTo   (g G.:*: h) = bTo   @rT g  :*:  bTo   @rT h
  bFrom (l  :*:  r) = bFrom @rT l G.:*: bFrom @rT r

instance BShaped rT rS G.U1 I where
  bTo   _ = I
  bFrom _ = G.U1

instance {-# overlappable #-}
         (G.Generic t, BShaped rT rS (G.Rep t) s)
      => BShaped rT rS (G.Rec0 t) s where
  bTo   = bTo @rT . G.from . G.unK1
  bFrom = G.K1 . G.to . bFrom @rT

instance BShaped rT rS (G.Rec0 t) (T t) where
  bTo   = T . G.unK1
  bFrom = G.K1 . unT

instance (G.Generic rT, BShaped rT rS (G.Rep rT) rS)
      => BShaped rT rS (G.Rec0 rT) F where
  bTo   = F . bTo @rT . G.from . G.unK1
  bFrom = G.K1 . G.to . bFrom @rT . unF

instance (G.Generic rT, Functor t, BShaped rT rS (G.Rep rT) rS)
      => BShaped rT rS (G.Rec0 (t rT)) (F1 t) where
  bTo   = F1 . fmap (bTo @rT . G.from) . G.unK1
  bFrom = G.K1 . fmap (G.to . bFrom @rT) . unF1

instance (G.Generic rT, Bifunctor t, BShaped rT rS (G.Rep rT) rS)
      => BShaped rT rS (G.Rec0 (t rT rT)) (F2 t) where
  bTo   = F2 . join bimap (bTo @rT . G.from) . G.unK1
  bFrom = G.K1 . join bimap (G.to . bFrom @rT) . unF2

instance {-# overlappable #-}
         TypeError (RepDoesn'tMatchShape rT rS g s) => BShaped rT rS g s where
  bTo   = error "bTo: unreachable branch"
  bFrom = error "bFrom: unreachable branch"

type RepDoesn'tMatchShape rT rS g s
      = 'Text "In blueprint"
  ':$$: 'Text "  for type:               " ':<>: 'ShowType rT
  ':$$: 'Text "  being matched on shape: " ':<>: 'ShowType rS
  ':$$: 'Text ""
  ':$$: 'Text "shape:"
  ':$$: 'Text ""
  ':$$: 'ShowType s
  ':$$: 'Text ""
  ':$$: 'Text "cannot be matched with the part that corresponds to"
  ':$$: 'Text ""
  ':$$: 'ShowType (GShapeOf g)
  ':$$: 'Text ""

-------------------------------------------------------------------------------
class    NonRecOf s s => NonRec s where
  refix :: DataOf t s -> DataOf u s

instance NonRecOf s s => NonRec s where
  refix = refixOf @s

class NonRecOf rS (s :: Shape) where
  refixOf :: DataOf t s -> DataOf u s

instance NonRecOf rS (T a) where
  refixOf (T a) = T a

instance TypeError (RecursiveShape rS  F    ) => NonRecOf rS  F     where
  refixOf = error "refix: unreachable branch"
instance TypeError (RecursiveShape rS (F1 t)) => NonRecOf rS (F1 t) where
  refixOf = error "refix: unreachable branch"
instance TypeError (RecursiveShape rS (F2 t)) => NonRecOf rS (F2 t) where
  refixOf = error "refix: unreachable branch"

instance NonRecOf rS I where
  refixOf I = I

instance (NonRecOf rS a, NonRecOf rS b) => NonRecOf rS (a * b) where
  refixOf (a :*: b) = refixOf @rS a :*: refixOf @rS b

instance (NonRecOf rS a, NonRecOf rS b) => NonRecOf rS (a + b) where
  refixOf (L a) = L $ refixOf @rS a
  refixOf (R b) = R $ refixOf @rS b

instance NonRecOf rS O where
  refixOf = \case {}

type RecursiveShape rS s
      = 'Text "Shape:"
  ':$$: 'Text ""
  ':$$: 'ShowType rS
  ':$$: 'Text ""
  ':$$: 'Text "contains recursive part"
  ':$$: 'Text ""
  ':$$: 'ShowType s
  ':$$: 'Text ""

-------------------------------------------------------------------------------
class MapShape s t u where
  mapShape :: (forall g. DataOf g t -> DataOf (MappedShape g t u) u)
           -> DataOf rS s
           -> DataOf (MappedShape rS t u) (MappedShape s t u)

instance {-# overlapping #-} MapShape t t u where
  mapShape = id

instance ( MapShape a t u
         , MapShape b t u
         , MappedShape (a * b) t u ~ (MappedShape a t u * MappedShape b t u)
         )
      => MapShape (a * b) t u where
  mapShape f (a :*: b) = mapShape f a :*: mapShape f b

instance ( MapShape a t u
         , MapShape b t u
         , MappedShape (a + b) t u ~ (MappedShape a t u + MappedShape b t u)
         )
      => MapShape (a + b) t u where
  mapShape f (L a) = L $ mapShape f a
  mapShape f (R b) = R $ mapShape f b

-- instance {-# overlappable #-} MapShape s t u where
--   mapShape f d = d

type family MappedShape (s :: Shape) (t :: Shape) (u :: Shape) :: Shape where
  MappedShape t       t u = u
  MappedShape (a * b) t u = MappedShape a t u * MappedShape b t u
  MappedShape (a + b) t u = MappedShape a t u + MappedShape b t u
  MappedShape s       _ _ = s